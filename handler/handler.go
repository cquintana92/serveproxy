package handler

import (
	"net/http"
	"net/http/httputil"
	"net/url"
	"path"
	"path/filepath"
	"serveproxy/log"
)

type RequestHandler struct {
	dirPath  string
	dir      http.Dir
	urlProxy *url.URL
	proxy    *httputil.ReverseProxy
}

func NewRequestHandler(dir http.Dir, dirPath string, proxy *url.URL) RequestHandler {
	return RequestHandler{
		dir:      dir,
		dirPath:  dirPath,
		urlProxy: proxy,
		proxy:    httputil.NewSingleHostReverseProxy(proxy),
	}
}

func (h *RequestHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		h.handleGetRequest(w, r)
	} else {
		h.proxyRequest(w, r)
	}
}

func (h *RequestHandler) handleGetRequest(w http.ResponseWriter, r *http.Request) {
	urlPath := r.URL.Path
	fullName := filepath.Join(h.dirPath, filepath.FromSlash(path.Clean("/"+urlPath)))
	f, err := h.dir.Open(urlPath)
	if err != nil {
		if urlPath == "" || urlPath == "/" {
			log.Logger.Infof("Converting [%s] to [/index.html]", urlPath)
			r.URL.Path = "/index.html"
			h.handleGetRequest(w, r)
		} else {
			h.proxyRequest(w, r)
		}
		return
	}

	if _, err := f.Stat(); err != nil {
		log.Logger.Errorf("Could not stat file [%s]: %+v", urlPath, err)
		http.Error(w, "Could not stat file", http.StatusInternalServerError)
	} else {
		log.Logger.Infof("Serving [%s]", urlPath)
		http.ServeFile(w, r, fullName)
	}
}

func (h *RequestHandler) proxyRequest(w http.ResponseWriter, req *http.Request) {

	log.Logger.Infof("Proxying [%s] [%s]", req.Method, req.URL.Path)

	req.URL.Host = h.urlProxy.Host
	req.URL.Scheme = h.urlProxy.Scheme
	req.Header.Set("X-Forwarded-Host", req.Header.Get("Host"))
	req.Host = h.urlProxy.Host
	h.proxy.ServeHTTP(w, req)
}
