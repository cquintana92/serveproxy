module serveproxy

go 1.14

require (
	github.com/jessevdk/go-flags v1.4.0
	github.com/logrusorgru/aurora v0.0.0-20200102142835-e9ef32dff381
	github.com/mattn/go-isatty v0.0.12
	github.com/sirupsen/logrus v1.6.0
)
