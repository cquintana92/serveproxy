.PHONY: help all build deps fmt clean

ARTIFACT_NAME:=serveproxy

SHELL:=/bin/bash
MAKEFILE_PATH := $(abspath $(lastword $(MAKEFILE_LIST)))
PROJECT_ROOT := $(dir $(MAKEFILE_PATH))
BIN_DIRECTORY := ${PROJECT_ROOT}/bin

GO:=go

DISABLE_CACHE:=-count=1
VERBOSE:=-v
TEST_COMMAND:=${GO} test ./...
TEST_NO_CACHE_COMMAND:=${TEST_COMMAND} ${DISABLE_CACHE}
BUILD_COMMAND:=${GO} build
SRC_DIR:=${PROJECT_ROOT}

VERSION=`git describe --tags --always --dirty`
VERSION_VARIABLE_NAME=main.Version
VERSION_VARIABLE_BUILD_FLAG=-ldflags "-X ${VERSION_VARIABLE_NAME}=${VERSION}"
BUILD_WITH_VERSION_COMMAND=${BUILD_COMMAND} ${VERSION_VARIABLE_BUILD_FLAG}

all: help

deps: ## Download all dependencies
	@ ${GO} get

build: deps ## Builds the binary
	@ ${BUILD_WITH_VERSION_COMMAND} -o ${BIN_DIRECTORY}/${ARTIFACT_NAME} main.go

version: ## Print the version
	@echo $(VERSION)


fmt: ## Apply linting and formatting
	@ ${GO} fmt ./...

clean: ## Remove all build artifacts
	@ rm -rf ${BIN_DIRECTORY}

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
