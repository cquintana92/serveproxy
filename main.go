package main

import (
	"fmt"
	"github.com/jessevdk/go-flags"
	"net/http"
	"net/url"
	"os"
	"serveproxy/constants"
	"serveproxy/handler"
	"serveproxy/log"
)

var Version = "0.1.2"

var (
	opts struct {
		LogLevel string `long:"loglevel" short:"l" default:"info" description:"LogLevel [TRACE, DEBUG, INFO, WARN, ERROR]"`
		Proxy    string `long:"proxy" short:"p" default:"" description:"URL where to proxy the requests"`
		Port     int    `long:"port" short:"P" default:"3000" description:"Port where the service will listen for HTTP connections"`
		Version  bool   `long:"version" short:"v" description:"Show version"`
		Args     struct {
			Path string `description:"Directory to be served" default:"."`
		} `positional-args:"yes"`
	}
)

func handleInitError(err error) {
	if flagsErr, ok := err.(*flags.Error); ok && flagsErr.Type == flags.ErrHelp {
		print(err.Error())
		os.Exit(0)
	} else {

		log.NewLogger("ERROR").Fatal(err.Error())
		os.Exit(1)
	}
}

func performInits() {
	parser := flags.NewParser(&opts, flags.Default^flags.PrintErrors)
	_, err := parser.Parse()
	if err != nil {
		handleInitError(err)
	}

	log.InitLogger(opts.LogLevel)
}

func main() {

	performInits()
	if opts.Version {
		message := fmt.Sprintf("%s (%s)", constants.NAME, Version)
		fmt.Println(message)
		return
	}

	if opts.Args.Path == "" {
		opts.Args.Path = "./"
	}

	log.Logger.Infof("Params: %+v", opts)

	dir := http.Dir(opts.Args.Path)
	proxyURL, err := url.Parse(opts.Proxy)
	if err != nil {
		log.Logger.Fatalf("Error parsing proxy URL: %+v", err)
	}
	httpHandler := handler.NewRequestHandler(dir, opts.Args.Path, proxyURL)
	http.Handle("/", &httpHandler)

	address := fmt.Sprintf(":%d", opts.Port)
	log.Logger.Infof("Listening on %s...", address)
	err = http.ListenAndServe(address, nil)
	if err != nil {
		log.Logger.Fatal(err)
	}
}
