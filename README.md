# ServeProxy

## What is it

ServeProxy is a command line utility that starts an HTTP server which serves a static file directory and proxies the rest of requests to a host of your choice.

That means:

- If a GET request is performed and there is a file which matches the requested path, the static file is served.
- If a GET request is performed and **there is not** a file which matches the requested path, the request is proxyed.
- Any other method is proxied directly.

When a request is proxied, all the request content is passed as-is (only adding the `X-Forwarded-Host` header to the request).

## Why?

This project was created because of a need in a workflow.

An application frontend is created in a different repository than the backend. When a release is made, the frontend files are built into static HTML/JS/CSS files, and the backend serves the result of that build.

The frontend has some acceptance tests that check the correct integration of the frontend with the backend.

When the frontend is being developed, a development webserver is used, with hot-reload capabilities and automatic request proxying (webpack dev server). However, in CI there was the need to connect a frontend to a backend that may be running in another server (or even connect it to multiple backend version in order to check for regressions). As the backend usually serves the frontend, the frontend performs the HTTP requests to `/`. This, of course, would not work by using a static file server, so it would not find the API calls. So, the only other option was running the development web server and pointing it to the desired backend.

This project aims to solve that issue: It's able to serve the frontend static files, and proxy any other request to the desired backend.

## How to use it

```
Usage:
  serveproxy [OPTIONS] [Path]

Application Options:
  -l, --loglevel= LogLevel [TRACE, DEBUG, INFO, WARN, ERROR] (default: info)
  -p, --proxy=    URL where to proxy the requests
  -P, --port=     Port where the service will listen for HTTP connections (default: 3000)
  -v, --version   Show version

Help Options:
  -h, --help      Show this help message

Arguments:
  Path:           Directory to be served
```

* Path: Optional. Defaults to the current working directory.
* Proxy: Root URL where the requests will be proxy-ed.
