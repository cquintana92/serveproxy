package log

import (
	"github.com/logrusorgru/aurora"
	"github.com/mattn/go-isatty"
	"os"
)

var (
	Aurora = aurora.NewAurora(true)
)

func isRunningInTerminal() bool {
	if isatty.IsTerminal(os.Stdout.Fd()) {
		return true
	} else if isatty.IsCygwinTerminal(os.Stdout.Fd()) {
		return true
	} else {
		return false
	}
}

func SetColorsEnabled(enabled bool) {

	if !enabled {
		Aurora = aurora.NewAurora(false)
		return
	}

	enableColors := true
	if !isRunningInTerminal() {
		Logger.Warn("Disabling colors because the program is not running in a terminal")
		enableColors = false
	} else if os.Getenv("NO_COLOR") != "" {
		Logger.Warn("Disabling colors because the NO_COLOR env variable is set")
		enableColors = false
	}

	Aurora = aurora.NewAurora(enableColors)
}
